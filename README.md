# Solarmod-code

Semi-analytical model for solar modulation in two dimensions.

A minimal test script is shown in test.py in python and test.cpp in c++.  In
both cases the local interstellar spectrum is loaded from a file, the spectrum
is modulated using the parametrisation presented in arXiv:1909.01154 and given
scale factors g1 and g2 and then write the modulated spectrum to a file.
