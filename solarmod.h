#ifndef solarmod_h
#define solarmod_h
#include <vector>

class Heliosphere{
  double B0_;
  double kappaA0_;
  double V0_;
  int Z_;
  double me_;
  
  public:
  Heliosphere();
  double B(double r);
  double drift(double r, double p);
  double diff(double r, double p);
  double V(double p, double a, double b);
  double get_pLIS(double pTOA,double r, double a, double b, double fudge);
  void modulate_EFF(double k0,double fac,double a,double b);
  
  std::vector<double> pTOA_;
  std::vector<double> pLIS_;
  std::vector<double> LIS_;
  std::vector<double> Spectrum_;
  
  class dpdr_functor{
    public:
    double a_;
    double b_;
    double g1_;
    Heliosphere &h_;
    dpdr_functor(Heliosphere &h);
    void operator() (const std::vector<double> &p, std::vector<double> &dpdr, const double r);
  };
  dpdr_functor dpdr_functor_;

};
#endif
