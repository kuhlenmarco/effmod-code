import numpy as np
from scipy.integrate import solve_ivp

class heliosphere:

    def __init__(self):
        self.B0 = 10.                ## magnetic field strength
        self.kappaA0 = 1.            ## drift coefficient
        self.V0 = 400. * 0.000577548 ## solar wind speed in AU day^-1
        self.Z = 1                   ## particle charge
        self.me = 5.11e-4            ## particle mass

    def B(self,r):
        return self.B0


    def drift(self,r, p):
        return self.kappaA0 * p / (3. * self.B(r)) * (10. * pow(p, 2.)) / (1. + 10. * pow(p, 2.))

    def diff(self,r, p):
        a = 0.
        b = 1.55
        c = 3.5
        Rk = 0.32
        result = (10./self.B(r))
        result *= pow(p, a)
        result *= pow((pow(p,c) + pow(Rk,c)) / (1. + pow(Rk,c)), (b-a)/c)
        return result

    def V(self,p,a,b=0.8):
        theta = np.ones_like(p)
        pb = b
        theta[p<pb] = a
        theta[p>pb] = 1.5
        return self.V0*theta


    def modulate_FF(self, loglogfun, T, phi):
        """
        Returns solar-modulated flux in force-field approximation
        """
        if (phi<0): phi=-phi
        return T*(T+2*me) / (T+Z*phi) / (T+Z*phi + 2*me) * 10**loglogfun(np.log10((T+Z*phi)))


    def dpdr(self,r, p,a,b, g1):
        return g1 * p * self.V(p,a,b)/ 3. / self.diff(r, p)


    def get_pLIS(self, pTOA,a,b, g1):
        pLIS = solve_ivp(lambda r, p: self.dpdr(r, p,a,b, g1), [0., 120.], pTOA, t_eval=np.linspace(0.5, 119.5, 120),method='LSODA').y
        return pLIS

    def modulate_EFF(self, loglogfun, pTOA, pars):
        """
        Returns solar-modulated flux in extended force-field approximation
        """
        g1, g2,a,b = pars
        pLIS = self.get_pLIS(pTOA,a,b, g1)
        expfactor = np.exp(np.sum(-g2* self.drift(-99, pLIS)/self.diff(-99, pLIS), axis=1))
        return pow(pTOA/pLIS[:, -1], 2.) * 10**loglogfun(np.log10(pLIS[:, -1])) * expfactor
