import numpy as np
import matplotlib.pyplot as plt

lisfile = np.genfromtxt("LIS.txt")
result = np.genfromtxt("result.txt")
resultpy = np.genfromtxt("resultpy.txt")

plt.plot(lisfile[:,0],lisfile[:,1])
plt.plot(lisfile[:,0],result,label='c++')
plt.plot(lisfile[:,0],resultpy,label='python')
plt.yscale('log')
plt.xscale('log')
plt.legend()
plt.show()
