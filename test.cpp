#include "solarmod.h"
#include <fstream>
#include <vector>
#include <iostream>

int main(){
  std::fstream lisfile("Vittino_et_al_e-_LIS.txt");
  double tmp1;
  double tmp2;
  Heliosphere h; //define an object of the class to modulate the interstellar spectrum
  
  while (lisfile >> tmp1 >> tmp2){  //read interstellar spectrum and corresponding momenta from file and save in the member vectors of the heliosphere class
    h.pTOA_.push_back(tmp1);
    h.LIS_.push_back(tmp2);
  }

  h.modulate_EFF(0.0338,0.07036,1.76444679,0.54649172); //modulate the local interstellar spectrum
  
  std::ofstream output("result.txt");
  for (auto spec: h.Spectrum_){  //save the calculated interstellar spectrum to file
    output << spec << std::endl;
  }
  output.close();
  return 0;
}
