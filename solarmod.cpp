#include "solarmod.h"
#include <cmath>
#include <boost/numeric/odeint.hpp>
#include <boost/math/interpolators/barycentric_rational.hpp>

Heliosphere::Heliosphere(): B0_(10.),kappaA0_(1.),V0_(400*0.000577548),Z_(1),me_(5.11e-4),dpdr_functor_(*this){
}

double Heliosphere::B(double r){
  /* 
   * Returns the magnetic field as a function of the radial coordinate.
   */
  return B0_;
}

double Heliosphere::drift(double r, double p){
  /*
   * Returns the drift coefficient kappa in the transport equation.
   */
  return kappaA0_*p/(3.*B(r))*(10.*p*p)/(1.+10.*p*p);
}

double Heliosphere::diff(double r, double p){
  /*
   * Returns the diffusion coefficient in radial direction.
   */
  double a=0;
  double b=1.55;
  double c=3.5;
  double test=1.;
  double Rk=0.32;
  double result = test;
  result *= 10./B(r);
  result *= std::pow(p,a);
  result *= std::pow((std::pow(p,c)+std::pow(Rk,c))/(1+std::pow(Rk,c)),(b-a)/c);
  return result;
}

double Heliosphere::V(double p, double a, double b){
  /*
   * Returns the radial component of the solar wind averaged with the derivative
   * of the phase space density. Therefore it is a function of momentum.
   */
  if (p>b){
    return V0_*1.5;
  }
  else{
    return V0_*a;
  }
}

Heliosphere::dpdr_functor::dpdr_functor(Heliosphere &heliosphere) : h_(heliosphere){}

void Heliosphere::dpdr_functor::operator() (const std::vector<double> &p, std::vector<double> &dpdr, const double r){
  /* 
   * Calculates the term for dp/dr for the method of characteristics and saves
   * it in output vector. 
   */
  dpdr[0] = g1_*p[0]*h_.V(p[0],a_,b_)/3./h_.diff(r,p[0]);
  }


double Heliosphere::get_pLIS(double pTOA, double r,double a, double b, double g1){
  /*
   * Solves the ordinary differential equation and returns the momentum 
   * of the particle at some radial distance r.
   */
  dpdr_functor_.a_ = a;
  dpdr_functor_.b_ = b;
  dpdr_functor_.g1_ = g1;
  std::vector<double> pLIS(1);
  pLIS[0] =  pTOA;
  size_t steps = boost::numeric::odeint::integrate(dpdr_functor_, pLIS ,0.5 ,r ,0.01);
  return pLIS[0];
}

void Heliosphere::modulate_EFF(double g1, double g2, double a, double b){
  /*
   * Takes the four parameters of the model and calculates the modulated spectrum. 
   * The result is then saved in the Spectrum_ vector.
   */

  for (auto p: pTOA_){
    double pLIS = get_pLIS(p,119.5,a,b,g1);
    pLIS_.push_back(pLIS);
  }
  std::vector<double> expfactor;
  double precision = 1;
  for (int i=0; i<pLIS_.size();i++){
    double expfactortmp = 0.;
    for (int j=0;j<int(119/precision);j++){
      double r = j*precision+0.5;
      double pLIS = get_pLIS(pTOA_[i],r,a,b,g1);
      expfactortmp += -g2*drift(-99,pLIS)/diff(-99,pLIS);
    }
    expfactor.push_back(std::exp(expfactortmp));
  }
  std::vector<double> loglis;
  std::vector<double> logptoa;
  for (int i=0; i<pLIS_.size();i++){
    loglis.push_back(std::log(LIS_[i]));
    logptoa.push_back(std::log(pTOA_[i]));
  }
  boost::math::barycentric_rational<double> LIS(logptoa.data(),loglis.data(),LIS_.size());
  for (int i=0; i<pLIS_.size();i++){
    Spectrum_.push_back(pTOA_[i]*pTOA_[i]/pLIS_[i]/pLIS_[i]*expfactor[i]*std::exp(LIS(std::log(pLIS_[i]))));
  }
}

