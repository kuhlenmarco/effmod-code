import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
import solarmod


## LIS fluxes from Vittino et al. [arXiv:1904.05899]
elpos = np.loadtxt("Vittino_et_al_e-_LIS.txt")
Es = elpos[:, 0] ## e- energy in GeV
y = elpos[:,1] ## e- flux in (GeV m^2 s sr)^-1
loglogf_ele = interp1d(np.log10(Es), np.log10(y), fill_value='extrapolate') ## interpolate double-logarithmically
plt.plot(Es, y, 'k-', label="LIS")


## AMS e- flux for BR 2426
data_AMS_ele = np.loadtxt("ele_AMS_PRL2018_ekin_000.txt")
plt.errorbar(data_AMS_ele[:, 0], data_AMS_ele[:, 3], yerr=data_AMS_ele[:, 4], fmt='.', label="AMS Data, BR 2426" )


## semi-analytical model
h = solarmod.heliosphere()
bf_ele = [0.0338, 0.07036, 1.76444679, 0.54649172] ## model parameters g1, g2, deltaV, Rk
x = np.logspace(-2, 2, 100)
model_ele = h.modulate_EFF(loglogf_ele, x, bf_ele)
plt.plot(x, model_ele, '-', label="semi-analytical model")


plt.xscale('log')
plt.yscale('log')
plt.legend(ncol=2, fontsize=8)
plt.show()

np.savetxt('resultpy.txt', model_ele)
